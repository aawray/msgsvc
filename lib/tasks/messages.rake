namespace :messages do
  desc 'Send all pending messages'
  task send: :environment do
    Message.transaction do
      query = "UPDATE messages SET status_code = '#{MessageStatus::Sending}' WHERE status_code = '#{MessageStatus::Pending}' RETURNING id"
      rows = ActiveRecord::Base.connection.execute(query)
      rows.each { |row|
        message = Message.where(id: row['id']).first
        message.dispatch
        message.save
      }
    end
  end

  desc 'Archive old messages'
  task archive: :environment do
      query = <<-SQL
        WITH m AS (DELETE FROM messages WHERE status_code IN ('failed', 'sent') RETURNING *)
        INSERT INTO archived_messages SELECT * FROM m;
      SQL
      ActiveRecord::Base.connection.execute(query)
  end
end
