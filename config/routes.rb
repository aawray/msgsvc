Rails.application.routes.draw do
  # apipie if Rails.env.development?
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'application#root'
  get '/404', to: 'application#handle_error_404'
  get '/500', to: 'application#handle_error_500'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resource :auth
      resources :message_adapters, only: %i[index]
      resources :messages
    end
  end
end
