module Api::V1
  class ApiController < ApplicationController
    resource_description do
      path '/v1'
      api_version :v1
    end

    before_action :init
    before_action :authorize_apikey

    def init
      Session.token = request.headers[:token]
    end

    def authenticate_user
      Session.user = User.with_token(Session.token).first if Session.token.present?
    end

    def authorize_user
      authenticate_user
      raise Error::Unauthorized.new if !Session.authorized?
    end

    def authorize_apikey
      request.headers[:apikey] == Rails.application.credentials.apikey
    end
  end
end
