module Api::V1
  class MessagesController < ApiController
    before_action :authorize_user
    before_action :init_message, only: %i[show update destroy]
    
    def index
      messages = Session.user.messages.all
      if params[:page].present?
        messages = messages.page(params[:page]).per(params[:limit] || 10)
      end
      respond messages
    end

    def create
      body, urgent, recipients = params[:body], params[:urgent], params[:recipients]
      raise Error::BadRequest.new code: 'MESSAGE_RECIPIENTS_INVALID' if recipients.blank?
      raise Error::BadRequest.new code: 'MESSAGE_BODY_INVALID' if body.blank?
      Message.transaction do
        recipients.each do |recipient|
          payload = {
            recipient_id: recipient[:id],
            adapter_code: recipient[:adapter],
            body: body,
            urgent: urgent
          }
          @message = Session.user.messages.new payload
          begin
            @message.save
          rescue ActiveRecord::RecordNotUnique
            raise Error::BadRequest.new code: 'MESSAGE_EXISTS'
          end
        end
      end
    end

    def show
      respond @message
    end

    def update
      raise Error::NotImplemented.new
    end

    def destroy
      raise Error::Forbidden.new if @message.code != MessageStatus::Pending
      Message.transaction do
        query = "DELETE FROM messages WHERE id = #{id.to_i} AND status_code = '#{MessageStatus::Pending}' RETURNING id"
        rows = ActiveRecord::Base.connection.execute(query)
        raise Error::Conflict.new if rows&.dig(0, 'id') != @message.id
      end
    end

    private

    def init_message
      @message = Session.user.messages.where(id: params[:id]).first
      raise Error::NotFound.new if @message.blank?
    end

    def message_params
      params.permit(:body, :urgent, recipients: [[:adapter, :id]])
    end
  end
end
