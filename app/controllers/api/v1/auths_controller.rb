module Api::V1
  class AuthsController < ApiController
    TOKENS_ITERATIONS_LIMIT = 10
    TOKENS_PER_USER_LIMIT = 5

    before_action :authorize_user, only: %i[show]
    before_action :init_user_token, only: %i[update destroy]
  
    api :GET, '/v1/auth'
    returns code: 200
    returns code: 401
    returns code: 403
    def show
      respond(Session.user.as_json.select{ |k| %w[status_code created_at].include? k })
      # head :ok unless
    end
  
    api :POST, '/v1/auth'
    param :phone, String, required: true
    param :code, String, default_value: '7331'
    returns code: 200 do
      param :code do
        param :expires_at, String
        param :expire_seconds, Integer
      end
    end
    returns code: 200 do
      param :token, String
    end
    returns code: 400
    returns code: 403
    def create
      adapter = params[:adapter]
      case adapter
      when 'phone_code'
        auth_phone_code
      when 'example_hash'
        auth_example_hash
      else
        raise Error::BadRequest.new code: 'AUTH_ADAPTER_NOT_FOUND'
      end
    end
  
    api :PUT, '/v1/auth'
    returns code: 204
    returns code: 401
    returns code: 403
    def update
      @user_token.update updated_at: Time.current
    end
  
    api :DELETE, '/v1/auth'
    returns code: 204
    returns code: 401
    returns code: 403
    def destroy
      @user_token.destroy
    end
  
    private
  
    def init_user_token
      @user_token = UserToken.where(token: Session.token, api_type: Session.type).order(nil).first
      raise Error::Unauthorized.new if @user_token.blank?
    end

    def auth_example_hash
      
    end

    def auth_phone_code
      phone = params[:phone]
      code = params[:code]
  
      raise Error::BadRequest.new code: 'PARAMS_PHONE_MISSING' if phone.nil?
  
      phone.gsub!(/[^0-9]/, '')
      raise Error::BadRequest.new code: 'PARAMS_PHONE_INVALID' unless phone.length > 10
      
      Session.user = User.where(phone: phone).first
      raise Error::Unauthorized.new if Session.user.present? && Session.user.status_code == UserStatus::Blocked
  
      if code.nil?
        phone_code = PhoneCode.request phone
        render json: {
          code: {
            expires_at: phone_code.expires_at,
            expire_seconds: (phone_code.expires_at - Time.current).to_i,
          }
        }
      else
        PhoneCode.check phone, code
  
        Session.user = User.create!(phone: phone, status_code: UserStatus::Registered) if Session.user.blank?
  
        user_tokens = Session.user.user_tokens.order(updated_at: :asc)
        user_tokens.first.destroy if user_tokens.size > TOKENS_PER_USER_LIMIT
  
        token_coincidence_count = 0
        user_token = nil
        until user_token.present?
          begin
            user_token = Session.user.user_tokens.create!(
              token: Session.generate_token,
              http_remote_addr: request.env['HTTP_X_FORWARDED_FOR'] || request.remote_ip,
              http_user_agent: request.headers['HTTP_USER_AGENT']
            )
          rescue ActiveRecord::RecordNotUnique
            token_coincidence_count += 1
            raise Error::InternalServerError.new code: 'USER_TOKEN_NOT_CREATED' if token_coincidence_count > TOKENS_ITERATIONS_LIMIT
          end
        end
  
        Session.token = user_token[:token]
    
        render json: {
          token: Session.token
        }
      end
    end
  end
end
