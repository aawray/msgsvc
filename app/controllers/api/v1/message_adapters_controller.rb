module Api::V1
  class MessageAdaptersController < ApiController
    before_action :authorize_user
    
    def index
      respond(MessageAdapter.where(archived_at: nil).map{ |adapter|
        adapter.as_json.select{ |k| %w[title code].include? k }
      })
    end
  end
end
