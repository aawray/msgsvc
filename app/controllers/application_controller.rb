class ApplicationController < ActionController::API
  include Concerns::HttpError

  resource_description do
    formats ['json']
  end

  rescue_from StandardError do |e|
    log_error e
    render_response Error::InternalServerError.new code: 'INTERNAL_SERVER_ERROR', text: e.message
  end
  
  rescue_from Error::CustomError do |e|
    render_response e
  end
  
  rescue_from ActiveRecord::ActiveRecordError do |e|
    render_activerecord_error e
  end

  rescue_from ActionController::ActionControllerError do |e|
    log_error e
    render_actioncontroller_error e
  end

  def root
    uptime_f = IO.read('/proc/uptime').split[0].to_f
    respond({
      uptime: uptime_f
    })
  end

  def handle_error_404
    raise Error::NotFound.new
  end

  def handle_error_500
    raise Error::InternalServerError.new
  end

  def respond(data, status = :ok)
    render json: data, status: status
  end

  def render_response e
    resp = {}
    if e.code.present?
      resp[:error_code] = e.code
      resp[:error_text] = I18n.t "errors.#{e.code}", default: ''
      if resp[:error_text].blank?
        resp[:error_text] = resp[:error_code]
      elsif e.vars.is_a? Hash
        resp[:error_text] = sprintf(resp[:error_text], e.vars)
      end
    end
    if e.text.present?
      resp[:error_text] = e.text
      if e.code.blank?
        resp[:error_code] = 'UNKNOWN_ERROR'
      end
    end
    if resp.blank?
      head e.class.const_get(:STATUS)
    else
      render json: resp, status: e.class.const_get(:STATUS)
    end
  end

  def render_activerecord_error e
    if e.is_a? ActiveRecord::StatementInvalid
      begin
        error = JSON.parse((e.to_s[/(ОШИБКА|ERROR):(.*)$/, 2]||'').strip)
        if error['code'].blank?
          log_error e
          error['code'] = 'DATABASE_UNKNOWN_ERROR'
        end
      rescue JSON::ParserError => _
        log_error e
        error = { 'code' => 'DATABASE_CRITICAL_ERROR' }
      end
      render_response Error::BadRequest.new code: error['code'], text: error['text'], vars: error['vars']
    else
      log_error e
      render_response Error::BadRequest.new code: e.class.to_s.upcase.sub(/:+/, '_')
    end
  end

  def render_actioncontroller_error e
    code = e.class.to_s
    text = e.message if e.message != code
    render_response Error::BadRequest.new code: code, text: text
  end

  def log_error e
    p e.message
    p e.backtrace
  end
end
