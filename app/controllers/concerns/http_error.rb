module Concerns::HttpError
  extend ActiveSupport::Concern
  
  module Error
    class CustomError < StandardError
      attr_reader :code
      attr_reader :text
      attr_reader :vars
      def initialize text: nil, code: nil, vars: nil
        @text = text
        @code = code
        @vars = vars
        super
      end
    end
    class Ok < CustomError
      STATUS = :ok
    end
    class BadRequest < CustomError
      STATUS = :bad_request
    end
    class Unauthorized < CustomError
      STATUS = :unauthorized
    end
    class PaymentRequired < CustomError
      STATUS = :payment_required
    end
    class Forbidden < CustomError
      STATUS = :forbidden
    end
    class NotFound < CustomError
      STATUS = :not_found
    end
    class Conflict < CustomError
      STATUS = :conflict
    end
    class InternalServerError < CustomError
      STATUS = :internal_server_error
    end
    class NotImplemented < CustomError
      STATUS = :not_implemented
    end
  end
end
