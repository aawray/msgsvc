class User < ApplicationRecord
  has_many :user_tokens
  has_many :messages

  validate :create_validator, on: :create
  validate :create_validator, on: :update

  scope :with_token, ->(token) {
    joins(:user_tokens)
    .where(user_tokens: { token: token })
    .where.not(users: { status_code: UserStatus::Blocked })
    .order(nil)
    .select('users.*')
  }

  def create_validator
  end
end
