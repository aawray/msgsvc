class ApplicationRecord < ActiveRecord::Base
  include Concerns::HttpError
  self.abstract_class = true
end
