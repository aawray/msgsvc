class UserStatus
  CODES = %w[registered blocked].freeze

  CODES.each do |code|
    const_set code.capitalize, code
  end
end
