class Message < ApplicationRecord
  belongs_to :message_adapter
  belongs_to :user

  before_validation :before_validation_on_create_trigger, on: :create
  
  validate :create_validator, on: :create

  before_update :before_update_trigger
  after_create :after_create_trigger

  attr_accessor :adapter_code
  attr_accessor :urgent

  after_initialize do
    self.status_code = MessageStatus::Pending
    self.recipient_id.strip!
    if message_adapter.blank? && adapter_code.present?
      self.message_adapter = MessageAdapter.where(code: adapter_code, archived_at: nil).first
      self.message_adapter_id = message_adapter.id
    end
    case message_adapter.code
    when 'badapter'
      self.extend Badapter
    when 'godapter'
      self.extend Godapter
    else
      raise Error::BadRequest.new code: 'MESSAGE_ADAPTER_INVALID'
    end
  end

  def before_validation_on_create_trigger
  end

  def create_validator
    raise Error::BadRequest.new code: 'MESSAGE_STATUS_CODE_INVALID' if !MessageStatus::CODES.include? status_code
    
    adapter_recipient_validator
    adapter_body_validator
  end

  def before_update_trigger
    if status_code_changed? && ([MessageStatus::Sent, MessageStatus::Failed].include? status_code)
      sent_count = status_code == MessageStatus::Sent ? 1 : 0
      failed_count = status_code == MessageStatus::Failed ? 1 : 0
      query = <<-SQL
        INSERT
        INTO message_recipient_stats (message_adapter_id, recipient_id, sent_count, failed_count, created_at, updated_at)
        VALUES (#{message_adapter_id}, '#{recipient_id}', #{sent_count}, #{failed_count}, NOW(), NOW())
        ON CONFLICT(message_adapter_id, recipient_id) DO
        UPDATE
        SET sent_count = message_recipient_stats.sent_count + #{sent_count},
            failed_count = message_recipient_stats.failed_count + #{failed_count}
      SQL
      ActiveRecord::Base.connection.execute(query)
    end
  end

  def after_create_trigger
    if urgent
      dispatch
      save
    end
  end

  def dispatch
    self.status_code = MessageStatus::Sent
    begin
      adapter_dispatch
    rescue CustomError => e
      self.status_code = MessageStatus::Failed
      # TODO: Custom notification
      p e
    end
  end

end

module Badapter
  include Concerns::HttpError
  
  def adapter_recipient_validator
    raise Error::BadRequest.new code: 'MESSAGE_RECIPIENT_ID_INVALID' if recipient_id.blank?
  end

  def adapter_body_validator
    raise Error::BadRequest.new code: 'MESSAGE_BODY_INVALID' if body.blank?
    raise Error::BadRequest.new code: 'MESSAGE_BODY_SEED_INVALID' if body.dig(:seed) =~ /^[0-9]{1,5}[a-z]{1,5}$/
  end

  def adapter_dispatch
    raise Error::BadRequest.new code: 'MESSAGE_NOT_SENT_BADAPTER_ERROR'
  end
end

module Godapter
  include Concerns::HttpError

  def adapter_recipient_validator
    raise Error::BadRequest.new code: 'MESSAGE_RECIPIENT_ID_INVALID' if recipient_id.blank? 
    raise Error::BadRequest.new code: 'MESSAGE_RECIPIENT_ID_INVALID' if !recipient_id =~ /^[0-9a-zA-Z]{6,32}$/
    raise Error::BadRequest.new code: 'MESSAGE_RECIPIENT_ID_INVALID' if recipient_id.chars.uniq.size < 4
  end

  def adapter_body_validator
    raise Error::BadRequest.new code: 'MESSAGE_BODY_INVALID' if body.blank?
    p body
    raise Error::BadRequest.new code: 'MESSAGE_BODY_TEXT_INVALID' if body['text'].blank?
    raise Error::BadRequest.new code: 'MESSAGE_BODY_TEXT_TOO_LONG' if body['text'].size > 32
  end

  def adapter_dispatch
    open('/tmp/msgsvc.txt') { |f| f << "#{Time.current} #{body.text}\n" }
  end
end
