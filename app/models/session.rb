class Session
  cattr_accessor :user
  cattr_accessor :token

  def self.authorized?
    user.present? && user.status_code == UserStatus::Registered
  end

  def self.generate_token
    SecureRandom.hex(32)
  end
end
