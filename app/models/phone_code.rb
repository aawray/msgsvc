class PhoneCode < ApplicationRecord
  PHONE_CODE_MAX_ATTEMPTS = 3
  PHONE_CODE_TIMEOUT = 2.minutes

  def self.request phone
    phone_code = PhoneCode.where(phone: phone).order(expires_at: :asc).first_or_initialize({ phone: phone, expires_at: Time.current })
    phone_code.generate_code_and_send_sms if phone_code.expires_at <= Time.current || phone_code.attempts <= 0
    phone_code
  end

  def self.check phone, code
    phone_code = PhoneCode.where('phone = ? AND attempts > 0 AND expires_at > ?', phone, Time.current).first
    raise Error::BadRequest.new code: 'PHONE_CODE_NOT_EXIST' if phone_code.blank?
      
    if phone_code.code != code
      PhoneCode.where(id: phone_code.id, expires_at: phone_code.expires_at).update_all('attempts = attempts - 1')
      raise Error::BadRequest.new code: 'PHONE_CODE_NOT_FOUND', text:"Код не найден (осталось #{phone_code.attempts} попыток)"
    end

    phone_code.destroy
  end

  def generate_code_and_send_sms
    generate_new_code
    send_sms
    save
  end

  def generate_new_code
    self.code = generate_code
    self.attempts = PHONE_CODE_MAX_ATTEMPTS
    self.expires_at = Time.current + PHONE_CODE_TIMEOUT
  end

  def generate_code
    rand(1000..9999).to_s
  end

  def send_sms
    sms_adapter = 'fakesms' # Rails.application.credentials.phone_code_sms_adapter
    case sms_adapter
    when 'fakesms'
      self.code = '0000'
    end
  end
end
