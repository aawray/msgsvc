class MessageStatus
  CODES = %w[pending sending failed sent].freeze

  CODES.each do |code|
    const_set code.capitalize, code
  end
end
