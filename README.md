# README

* Install vagrant: https://www.vagrantup.com/docs/installation/
* Run `vagrant up`
* Use Postman (collection and environment from `/doc`) or other browser to navigate the API
