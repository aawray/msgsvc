class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.references :user, foreign_key: true, null: false
      t.references :message_adapter, foreign_key: true, null: false
      
      t.string :recipient_id, null: false, limit: 512
      t.jsonb :body, null: false

      t.string :status_code, null: false
      
      t.timestamps
    end
    add_index :messages, [:message_adapter_id, :recipient_id, :body], unique: true

    status_codes = MessageStatus::CODES.map{ |t|quote t }.join(',')
    execute 'DROP TYPE IF EXISTS message_status;'
    execute "CREATE TYPE message_status AS ENUM (#{status_codes});"
    execute 'ALTER TABLE messages ALTER COLUMN status_code TYPE message_status USING status_code::message_status;'

    execute 'CREATE TABLE archived_messages (LIKE messages INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES);'
  end
end
