class CreateUserTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :user_tokens do |t|
      t.references :user, foreign_key: true
      t.string :token, null: false

      t.string :http_remote_addr, null: false
      t.string :http_user_agent, null: false

      t.timestamps
    end
  end
end
