class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :status_code, null: false, limit: 20

      t.string :phone, limit: 20, index: {unique: true}
      t.string :example_hash, limit: 128, index: {unique: true}

      t.timestamps
    end

    status_codes = UserStatus::CODES.map{ |t|quote t }.join(',')
    execute 'DROP TYPE IF EXISTS user_status;'
    execute "CREATE TYPE user_status AS ENUM (#{status_codes});"
    execute 'ALTER TABLE users ALTER COLUMN status_code TYPE user_status USING status_code::user_status;'
  end
end
