class CreatePhoneCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :phone_codes do |t|
      t.string :phone, null: false, limit: 20, index: {unique: true}
      t.string :code, limit: 8, null: false
      
      t.timestamp :expires_at
      t.integer :attempts, default: 0, null: false, limit: 2

      t.timestamps
    end
    add_index :phone_codes, [:phone, :code], unique: true
  end
end
