class CreateMessageAdapters < ActiveRecord::Migration[5.2]
  def change
    create_table :message_adapters do |t|
      t.string :title
      t.string :code
      t.timestamp :archived_at

      t.timestamps
    end
  end
end
