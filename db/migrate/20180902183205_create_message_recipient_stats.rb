class CreateMessageRecipientStats < ActiveRecord::Migration[5.2]
  def change
    create_table :message_recipient_stats do |t|
      t.references :message_adapter, foreign_key: true, null: false
      t.string :recipient_id, null: false
      
      t.integer :sent_count, default: 0, null: false
      t.integer :failed_count, default: 0, null: false

      t.timestamps
    end
    add_index :message_recipient_stats, [:message_adapter_id, :recipient_id], unique: true, name: :message_recipient_stat_uniq_idx
  end
end
