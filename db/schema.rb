# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_02_183205) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "message_adapters", force: :cascade do |t|
    t.string "title"
    t.string "code"
    t.datetime "archived_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "message_recipient_stats", force: :cascade do |t|
    t.bigint "message_adapter_id", null: false
    t.string "recipient_id", null: false
    t.integer "sent_count", default: 0, null: false
    t.integer "failed_count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_adapter_id"], name: "index_message_recipient_stats_on_message_adapter_id"
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "message_adapter_id", null: false
    t.string "recipient_id", limit: 512, null: false
    t.string "body", limit: 2048, null: false
    t.string "status_code", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_adapter_id", "recipient_id", "body"], name: "index_messages_on_message_adapter_id_and_recipient_id_and_body", unique: true
    t.index ["message_adapter_id"], name: "index_messages_on_message_adapter_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "phone_codes", force: :cascade do |t|
    t.string "phone", limit: 20, null: false
    t.string "code", limit: 8, null: false
    t.datetime "expires_at"
    t.integer "attempts", limit: 2, default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["phone", "code"], name: "index_phone_codes_on_phone_and_code", unique: true
    t.index ["phone"], name: "index_phone_codes_on_phone", unique: true
  end

  create_table "user_tokens", force: :cascade do |t|
    t.bigint "user_id"
    t.string "token", null: false
    t.string "http_remote_addr", null: false
    t.string "http_user_agent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_tokens_on_user_id"
  end

# Could not dump table "users" because of following StandardError
#   Unknown type 'user_status' for column 'status_code'

  add_foreign_key "message_recipient_stats", "message_adapters"
  add_foreign_key "messages", "message_adapters"
  add_foreign_key "messages", "users"
  add_foreign_key "user_tokens", "users"
end
