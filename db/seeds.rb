p PhoneCode.where(phone: '78901234567').first_or_create(code: '1234', expires_at: Time.current, attempts: 3)

p User.where(phone: '78901234567').first_or_create(status_code: UserStatus::Registered, example_hash: SecureRandom.hex(32))

p MessageAdapter.where(code: 'randapter').first_or_create(title: 'Random')
p MessageAdapter.where(code: 'badapter').first_or_create(title: 'Always failed')
p MessageAdapter.where(code: 'godapter').first_or_create(title: 'Always sent')
