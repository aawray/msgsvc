#!/bin/bash

export RAILS_ENV=production
cd /app

pacman -Scc
pacman -Syyu --noconfirm

pacman -S archlinux-keyring
pacman-key --refresh-keys

pacman -S --noconfirm base-devel nodejs postgresql ruby ruby-bundler rubygems

echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
localectl set-locale LANG=en_US.UTF-8

su - postgres -c "initdb --locale en_US.UTF-8 -E UTF8 -D '/var/lib/postgres/data'"

echo "host all all 0.0.0.0/0 md5" >> /var/lib/postgres/data/pg_hba.conf
echo "listen_addresses='*'" >> /var/lib/postgres/data/postgresql.conf
echo "unix_socket_directories = '/run/postgresql'" >> /var/lib/postgres/data/postgresql.conf

systemctl enable postgresql --now

su - postgres -c "psql -h localhost -f /app/vagrant/init.sql"

gem install bundler --no-rdoc --no-ri && bundle install --jobs 20 --retry 5

rails db:create
rails db:migrate
rails db:seed

puma -C config/puma.rb
